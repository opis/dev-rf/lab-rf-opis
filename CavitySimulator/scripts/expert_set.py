from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil


lvl1 = PVUtil.getString(pvs[0])
lvl2 = PVUtil.getString(pvs[1])
lvl3 = PVUtil.getString(pvs[2])
lvl4 = PVUtil.getString(pvs[3])

if lvl1=="" or lvl2 =="" or lvl3 == "" or lvl4 =="":
	ScriptUtil.getLogger().severe("At least one field is empty!")
	flag = 0
else:
	flag = 1

reference_flag = 0
if flag ==1:
	if lvl2 == "REFERENCE":
		if not 0<= int(lvl4) <=2:
			reference_flag = 1
	if reference_flag == 0:
		if lvl4 == "?":
			StringToSend = lvl1+":"+lvl2+":"+lvl3+lvl4
			PVUtil.writePV(str(pvs[5]), StringToSend, 500)
		else:
			StringToSend = lvl1+":"+lvl2+":"+lvl3+" "+lvl4
			PVUtil.writePV(str(pvs[6]), StringToSend, 500)
	else:
		ScriptUtil.getLogger().severe("Reference mode must be 0, 1 or 2")
