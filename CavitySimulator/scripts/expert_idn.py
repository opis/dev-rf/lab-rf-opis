from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil


value = PVUtil.getString(pvs[1])

if value == "":
	ScriptUtil.getLogger().severe("Field is empty!")    
	flag = 0
else:
	flag = 1

if flag ==1:
	PVUtil.writePV(str(pvs[2]), value, 500)
