from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
import csv


path_to_save = PVUtil.getString(pvs[19])
download_dir = path_to_save + "\Cavities_Model.txt"
csv_columns = ['Mode', 'Gain', 'Detuning', 'Q']

if path_to_save != "0":
	dict_data = [
	{'Mode': 1, 'Gain': PVUtil.getLong(pvs[0]), 'Detuning': PVUtil.getLong(pvs[1]), 'Q': PVUtil.getLong(pvs[2])},
	{'Mode': 2, 'Gain': PVUtil.getLong(pvs[3]), 'Detuning': PVUtil.getLong(pvs[4]), 'Q': PVUtil.getLong(pvs[5])},
	{'Mode': 3, 'Gain': PVUtil.getLong(pvs[6]), 'Detuning': PVUtil.getLong(pvs[7]), 'Q': PVUtil.getLong(pvs[8])},
	{'Mode': 4, 'Gain': PVUtil.getLong(pvs[9]), 'Detuning': PVUtil.getLong(pvs[10]), 'Q': PVUtil.getLong(pvs[11])},
	{'Mode': 5, 'Gain': PVUtil.getLong(pvs[12]), 'Detuning': PVUtil.getLong(pvs[13]), 'Q': PVUtil.getLong(pvs[14])},
	{'Mode': 6, 'Gain': PVUtil.getLong(pvs[15]), 'Detuning': PVUtil.getLong(pvs[16]), 'Q': PVUtil.getLong(pvs[17])}
	]

	# saving values to file
	try:
		with open(download_dir, 'wb') as csvfile:
			writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
			writer.writeheader()
			for data in dict_data:
				writer.writerow(data)
		ScriptUtil.getLogger().severe("Data has been saved")
	# exceptions
	except IOError:
		ScriptUtil.getLogger().severe("I/O error")
	except:
		ScriptUtil.getLogger().severe("Unknown Error")
else:
	ScriptUtil.getLogger().severe("Empty directory")