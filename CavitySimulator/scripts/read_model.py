from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
import csv


# directory
file_path = PVUtil.getString(pvs[0])
if len(file_path)>2:
	try:
		# gets data from txt file
		data = [row for row in csv.reader(open(file_path, 'r'))]

		# sets flag
		is_digit = True

		# checks if in the file are only numbers
		for i in range(1, len(data)):
			# lstrip to check also negative values
			for k in range(len(data[i])):
				if data[i][k].lstrip("-").isdigit() is False and data[i][k] != "":
					is_digit = False
					break
					
		# checks if value is a number (is digit alert is not false)
		if is_digit == False:
			raise ValueError
		for i in range(1, len(data)):
			if (data[i][0]) == '1':
				PVUtil.writePV(str(pvs[2]), data[i][1], 500)
				PVUtil.writePV(str(pvs[3]), data[i][2], 500)
				PVUtil.writePV(str(pvs[4]), data[i][3], 500)
			elif data[i][0] == '2':	
				PVUtil.writePV(str(pvs[5]), data[i][1], 500)
				PVUtil.writePV(str(pvs[6]), data[i][2], 500)
				PVUtil.writePV(str(pvs[7]), data[i][3], 500)
			elif data[i][0] == '3':
				PVUtil.writePV(str(pvs[8]), data[i][1], 500)
				PVUtil.writePV(str(pvs[9]), data[i][2], 500)
				PVUtil.writePV(str(pvs[10]), data[i][3], 500)
			elif data[i][0] == '4':
				PVUtil.writePV(str(pvs[11]), data[i][1], 500)
				PVUtil.writePV(str(pvs[12]), data[i][2], 500)
				PVUtil.writePV(str(pvs[13]), data[i][3], 500)
			elif data[i][0] == '5':
				PVUtil.writePV(str(pvs[14]), data[i][1], 500)
				PVUtil.writePV(str(pvs[15]), data[i][2], 500)
				PVUtil.writePV(str(pvs[16]), data[i][3], 500)
			elif data[i][0] == '6':
				PVUtil.writePV(str(pvs[17]), data[i][1], 500)
				PVUtil.writePV(str(pvs[18]), data[i][2], 500)
				PVUtil.writePV(str(pvs[19]), data[i][3], 500)
				
		ScriptUtil.showMessageDialog(widget, "Values read from file")

	except IOError:
			ScriptUtil.getLogger().severe("I/O error")
	except ValueError:
			ScriptUtil.getLogger().severe("Forbidden character in file")
	except:
			ScriptUtil.getLogger().severe("Unknown error")
else:
	ScriptUtil.getLogger().severe("Empty path file")
