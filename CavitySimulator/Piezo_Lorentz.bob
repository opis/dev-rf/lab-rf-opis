<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>Piezo and Lorentz</name>
  <width>760</width>
  <height>710</height>
  <widget type="rectangle" version="2.0.0">
    <name>title-bar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>760</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>title</name>
    <class>TITLE</class>
    <text>Cavity Simulator</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>570</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>subtitle</name>
    <class>SUBTITLE</class>
    <text>Piezo and  Lorentz Force</text>
    <x>490</x>
    <y use_class="true">20</y>
    <width>250</width>
    <height use_class="true">30</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">2</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Menu_button</name>
    <actions>
      <action type="open_display">
        <file>Calibration_Reference.bob</file>
        <target>replace</target>
        <description>Calibration and EEPROM</description>
      </action>
      <action type="open_display">
        <file>Expert.bob</file>
        <target>replace</target>
        <description>Expert</description>
      </action>
      <action type="open_display">
        <file>main_menu.bob</file>
        <target>replace</target>
        <description>Main menu</description>
      </action>
      <action type="open_display">
        <file>Model.bob</file>
        <target>replace</target>
        <description>Model</description>
      </action>
      <action type="open_display">
        <file>Network.bob</file>
        <target>replace</target>
        <description>Network</description>
      </action>
      <action type="open_display">
        <file>Slow_Resonance_Drift.bob</file>
        <target>replace</target>
        <description>Slow Resonance Drift</description>
      </action>
      <action type="open_display">
        <file>Status.bob</file>
        <target>replace</target>
        <description>Status</description>
      </action>
      <action type="open_display">
        <file>Temperature.bob</file>
        <target>replace</target>
        <description>Temperature</description>
      </action>
    </actions>
    <text>Menu</text>
    <x>440</x>
    <y>650</y>
    <width>150</width>
    <height>40</height>
    <tooltip>Choose window</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Close_button</name>
    <actions>
      <action type="execute">
        <script file="EmbeddedPy">
          <text><![CDATA[from org.csstudio.display.builder.runtime.script import ScriptUtil

ScriptUtil.closeDisplay(widget)]]></text>
        </script>
        <description>Execute Script</description>
      </action>
    </actions>
    <text>Close</text>
    <x>610</x>
    <y>650</y>
    <width>110</width>
    <height>40</height>
    <tooltip>Close window</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Lorentz_group</name>
    <x>40</x>
    <y>400</y>
    <width>680</width>
    <height>230</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>Lorentz_rectangle</name>
      <width>680</width>
      <height>230</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Lorentz_label</name>
      <class>HEADER2</class>
      <text>Lorentz Force</text>
      <width>700</width>
      <height>40</height>
      <font use_class="true">
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="group" version="2.0.0">
      <name>LorentzRead_group</name>
      <x>20</x>
      <y>50</y>
      <height>160</height>
      <style>3</style>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <transparent>true</transparent>
      <widget type="rectangle" version="2.0.0">
        <name>LorentzRead_rectangle</name>
        <width>300</width>
        <height>160</height>
        <line_width>2</line_width>
        <line_color>
          <color name="GROUP-BORDER" red="150" green="155" blue="151">
          </color>
        </line_color>
        <background_color>
          <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
          </color>
        </background_color>
        <corner_width>10</corner_width>
        <corner_height>10</corner_height>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LorentzRead_label</name>
        <class>HEADER3</class>
        <text>Current Value</text>
        <width>300</width>
        <height>40</height>
        <font use_class="true">
          <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
          </font>
        </font>
        <foreground_color use_class="true">
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </foreground_color>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LorentRead_label</name>
        <text>Lorentz Force:</text>
        <x>50</x>
        <y>60</y>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>LorentRead_update</name>
        <pv_name>$(P)$(R)Lorentz-RB</pv_name>
        <x>160</x>
        <y>60</y>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <tooltip>Gain of the piezo output</tooltip>
      </widget>
      <widget type="action_button" version="3.0.0">
        <name>Lorent_button</name>
        <actions>
          <action type="write_pv">
            <pv_name>$(pv_name)</pv_name>
            <value>1</value>
            <description>Write PV</description>
          </action>
        </actions>
        <pv_name>$(P)$(R)LorentzBut-SP</pv_name>
        <text>Get</text>
        <x>100</x>
        <y>110</y>
        <width>110</width>
        <tooltip>Read current value of Lorentz compensation</tooltip>
      </widget>
    </widget>
    <widget type="group" version="2.0.0">
      <name>LorentzSet_group</name>
      <x>360</x>
      <y>50</y>
      <height>160</height>
      <style>3</style>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <transparent>true</transparent>
      <widget type="rectangle" version="2.0.0">
        <name>LorentzSet_rectangle</name>
        <width>300</width>
        <height>160</height>
        <line_width>2</line_width>
        <line_color>
          <color name="GROUP-BORDER" red="150" green="155" blue="151">
          </color>
        </line_color>
        <background_color>
          <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
          </color>
        </background_color>
        <corner_width>10</corner_width>
        <corner_height>10</corner_height>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LorentzSet_label</name>
        <class>HEADER3</class>
        <text>Set Value</text>
        <width>300</width>
        <height>40</height>
        <font use_class="true">
          <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
          </font>
        </font>
        <foreground_color use_class="true">
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </foreground_color>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LorentzWrite_label</name>
        <text>Lorentz Force Gain:</text>
        <x>20</x>
        <y>60</y>
        <width>130</width>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>2</vertical_alignment>
      </widget>
      <widget type="textentry" version="3.0.0">
        <name>Lorentz_entry</name>
        <pv_name>$(P)$(R)Lorentz-RB</pv_name>
        <x>160</x>
        <y>60</y>
        <width>90</width>
        <tooltip>Sets the gain of Lorentz Force</tooltip>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>PiezoGain_group</name>
    <x>40</x>
    <y>70</y>
    <width>680</width>
    <height>310</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>PiezoGain_rectangle</name>
      <width>680</width>
      <height>310</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>PiezoGain_label</name>
      <class>HEADER2</class>
      <text>Piezo Gain</text>
      <width>680</width>
      <height>40</height>
      <font use_class="true">
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Current_group</name>
      <x>20</x>
      <y>50</y>
      <height>240</height>
      <style>3</style>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <transparent>true</transparent>
      <widget type="rectangle" version="2.0.0">
        <name>Current_rectangle</name>
        <width>300</width>
        <height>240</height>
        <line_width>2</line_width>
        <line_color>
          <color name="GROUP-BORDER" red="150" green="155" blue="151">
          </color>
        </line_color>
        <background_color>
          <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
          </color>
        </background_color>
        <corner_width>10</corner_width>
        <corner_height>10</corner_height>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Current_label</name>
        <class>HEADER3</class>
        <text>Current Values</text>
        <width>300</width>
        <height>40</height>
        <font use_class="true">
          <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
          </font>
        </font>
        <foreground_color use_class="true">
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </foreground_color>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="action_button" version="3.0.0">
        <name>Get_button</name>
        <actions>
          <action type="write_pv">
            <pv_name>$(pv_name)</pv_name>
            <value>1</value>
            <description>Write PV</description>
          </action>
        </actions>
        <pv_name>$(P)$(R)PiezoBut-SP</pv_name>
        <text>Get</text>
        <x>100</x>
        <y>190</y>
        <width>110</width>
        <tooltip>Read current configuration of piezo</tooltip>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Output_update</name>
        <pv_name>$(P)$(R)PiezoO-RB</pv_name>
        <x>160</x>
        <y>60</y>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <tooltip>Gain of the piezo output in uV/Hz</tooltip>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Piezo1_update</name>
        <pv_name>$(P)$(R)Piezo1-RB</pv_name>
        <x>160</x>
        <y>100</y>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <tooltip>Readout how piezo 1 voltage affects the detuning in mHz/V</tooltip>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Piezo2_update</name>
        <pv_name>$(P)$(R)Piezo2-RB</pv_name>
        <x>160</x>
        <y>140</y>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <tooltip>Readout how piezo 2 voltage affects the detuning in mHz/V</tooltip>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Output_label</name>
        <text>Piezo Output:</text>
        <x>50</x>
        <y>60</y>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Piezo1_label</name>
        <text>Piezo 1:</text>
        <x>90</x>
        <y>100</y>
        <width>60</width>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>2</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Piezo2_label</name>
        <text>Piezo 2:</text>
        <x>90</x>
        <y>140</y>
        <width>60</width>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>2</vertical_alignment>
      </widget>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Set_group</name>
      <x>360</x>
      <y>50</y>
      <height>240</height>
      <style>3</style>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <transparent>true</transparent>
      <widget type="rectangle" version="2.0.0">
        <name>Set_rectangle</name>
        <width>300</width>
        <height>240</height>
        <line_width>2</line_width>
        <line_color>
          <color name="GROUP-BORDER" red="150" green="155" blue="151">
          </color>
        </line_color>
        <background_color>
          <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
          </color>
        </background_color>
        <corner_width>10</corner_width>
        <corner_height>10</corner_height>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Set_label</name>
        <class>HEADER3</class>
        <text>Set Values</text>
        <width>300</width>
        <height>40</height>
        <font use_class="true">
          <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
          </font>
        </font>
        <foreground_color use_class="true">
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </foreground_color>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Output_label</name>
        <text>Piezo Output:</text>
        <x>50</x>
        <y>60</y>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>2</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Piezo1_label</name>
        <text>Piezo 1:</text>
        <x>90</x>
        <y>100</y>
        <width>60</width>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="label" version="2.0.0">
        <name>Piezo2_label</name>
        <text>Piezo 2:</text>
        <x>90</x>
        <y>140</y>
        <width>60</width>
        <horizontal_alignment>2</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="textentry" version="3.0.0">
        <name>Output_entry</name>
        <pv_name>$(P)$(R)PiezoO-SP</pv_name>
        <x>160</x>
        <y>60</y>
        <width>90</width>
        <tooltip>Sets the gain of the piezo output in uV/Hz</tooltip>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
      <widget type="textentry" version="3.0.0">
        <name>Piezo1_entry</name>
        <pv_name>$(P)$(R)Piezo1-SP</pv_name>
        <x>160</x>
        <y>100</y>
        <width>90</width>
        <tooltip>Sets how piezo 1 voltage affects the detuning in mHz/V</tooltip>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
      <widget type="textentry" version="3.0.0">
        <name>Piezo2_entry</name>
        <pv_name>$(P)$(R)Piezo2-SP</pv_name>
        <x>160</x>
        <y>140</y>
        <width>90</width>
        <tooltip>Sets how piezo 2 voltage affects the detuning in mHz/V</tooltip>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
    </widget>
  </widget>
</display>
